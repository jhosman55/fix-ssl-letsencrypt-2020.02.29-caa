#!/bin/bash
# documentacion 
# https://forums.cpanel.net/threads/lets-encrypt-will-revoke-3-million-certificates-on-march-4-2020.668345/#post-2737697
# Check: https://checkhost.unboundtest.com/
# Dev @namsohj

for dominio in $(cat ssl.txt); 

do
  if grep -lir $dominio --exclude=*ssl.txt;
  then
        servidor=$(grep -lir $dominio --exclude=ssl.txt);
        echo "Servidor:" $servidor;
        echo $dominio;
        usuario=$(cat $servidor | grep $dominio | cut -d " " -f 2);
        echo "Usuario de cPanel:" $usuario;
        ssh -tt -p 2255 root@$servidor whmapi1 delete_ssl_vhost host=$dominio;
        ssh -tt -p 2255 root@$servidor /usr/local/cpanel/bin/autossl_check --user=$usuario;
  else
        echo "no encontre nada";
  fi
done;